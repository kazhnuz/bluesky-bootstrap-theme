const { src, dest, parallel } = require('gulp');
const include = require('gulp-include');
const sass = require('gulp-sass');

sass.compiler = require('node-sass');

function html() {
  return src('src/*.html')
    .pipe(include())
    .pipe(dest('dist'))
}

function css() {
  return src('src/scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('dist'))
}

function dep() {
  return src(['dep/**/*'])
    .pipe(dest('dist/dep'));

}

function img() {
  return src(['src/img/*'])
    .pipe(dest('dist/img'));

}

exports.html = html;
exports.css  = css;
exports.dep  = dep;
exports.img  = img;
exports.default = parallel(html, css, dep, img);
